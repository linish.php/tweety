-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 26, 2020 at 10:19 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel8_tweety`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING HASH
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

DROP TABLE IF EXISTS `follows`;
CREATE TABLE IF NOT EXISTS `follows` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `following_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`following_user_id`),
  KEY `follows_following_user_id_foreign` (`following_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`user_id`, `following_user_id`, `created_at`, `updated_at`) VALUES
(2, 5, NULL, NULL),
(2, 3, NULL, NULL),
(2, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `tweet_id` bigint(20) UNSIGNED NOT NULL,
  `liked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `likes_user_id_tweet_id_unique` (`user_id`,`tweet_id`),
  KEY `likes_tweet_id_foreign` (`tweet_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `tweet_id`, `liked`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-12-23 18:52:17', '2020-12-23 19:05:04'),
(2, 2, 71, 1, '2020-12-23 20:10:31', '2020-12-23 20:10:58'),
(3, 2, 33, 0, '2020-12-23 20:11:02', '2020-12-23 20:12:26'),
(4, 2, 34, 0, '2020-12-23 20:11:10', '2020-12-23 20:11:10'),
(5, 2, 35, 0, '2020-12-23 20:11:14', '2020-12-23 20:11:14'),
(6, 2, 143, 0, '2020-12-23 20:19:16', '2020-12-23 20:19:16'),
(7, 2, 63, 0, '2020-12-23 20:19:22', '2020-12-23 20:19:22'),
(8, 2, 62, 1, '2020-12-23 20:19:25', '2020-12-23 20:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_20_104515_create_tweets_table', 1),
(5, '2020_12_20_120407_create_follows_table', 1),
(7, '2020_12_24_000307_create_likes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
CREATE TABLE IF NOT EXISTS `tweets` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`id`, `user_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, '1st tweet', '2020-12-23 01:41:25', '2020-12-23 01:41:25'),
(2, 2, 'Another tweet', '2020-12-23 07:41:58', '2020-12-23 07:41:58'),
(3, 2, 'Quia reiciendis illum facere pariatur reprehenderit.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(4, 2, 'Odio vero vel rem autem itaque enim.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(5, 2, 'Aspernatur voluptate quo libero sunt sapiente temporibus quia.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(6, 2, 'Eum sit voluptatem quia qui aliquam.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(7, 2, 'Quas corporis placeat suscipit earum qui sunt.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(8, 2, 'Dolore iure sed dicta voluptas.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(9, 2, 'Earum qui nihil iure dolor.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(10, 2, 'Dolores eligendi eaque fugit harum reprehenderit in dolores.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(11, 2, 'Pariatur quo eaque facilis quis numquam esse ut enim.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(12, 2, 'Architecto mollitia rerum ipsum consequatur voluptatum ut.', '2020-12-23 07:52:31', '2020-12-23 07:52:31'),
(13, 2, 'Repellendus explicabo fuga facere aliquam totam enim qui.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(14, 2, 'Eaque et dignissimos autem voluptatum.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(15, 2, 'Et cumque doloremque quos dolorum fugiat velit.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(16, 2, 'Et deleniti quasi totam sint.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(17, 2, 'Quas porro quos culpa aspernatur.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(18, 2, 'Consectetur fuga neque voluptas accusantium hic sint.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(19, 2, 'Minima quia excepturi excepturi saepe maxime.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(20, 2, 'Quia itaque inventore unde autem repellat.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(21, 2, 'Non architecto voluptate veritatis ipsum sed et.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(22, 2, 'Fuga esse voluptatum officia ipsa eius.', '2020-12-23 07:52:40', '2020-12-23 07:52:40'),
(23, 1, 'Laudantium necessitatibus atque ut facere amet ut accusamus necessitatibus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(24, 1, 'Quas eos modi doloremque omnis sunt.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(25, 1, 'Ut quam sapiente eum fugiat quas consectetur blanditiis quia.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(26, 1, 'Et ea asperiores accusantium.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(27, 1, 'Est ipsum earum temporibus dolor.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(28, 1, 'Nisi perferendis est possimus repellendus natus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(29, 1, 'Qui facilis nulla quis impedit error corrupti ratione.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(30, 1, 'Commodi debitis fuga minima minus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(31, 1, 'Odio aut ut nam aut blanditiis esse perferendis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(32, 1, 'Amet officia doloribus incidunt voluptatibus ratione doloribus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(33, 2, 'Corporis quis et reprehenderit.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(34, 2, 'Aut sunt inventore perferendis possimus voluptatem voluptate porro.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(35, 2, 'Suscipit est quia vel sed ea delectus consequatur.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(36, 2, 'Aperiam nam magni molestias consectetur qui hic.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(37, 2, 'Itaque maxime ducimus iusto libero nostrum nostrum eius officiis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(38, 2, 'Labore rem qui illo deserunt iure eius.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(39, 2, 'Enim ipsa voluptate aut eveniet asperiores in illo.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(40, 2, 'Accusamus velit eligendi inventore quia mollitia.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(41, 2, 'Nemo rem sapiente officiis cupiditate.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(42, 2, 'Fugit ea delectus qui quia qui quis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(43, 3, 'Aut dolorum sunt deleniti.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(44, 3, 'Debitis porro voluptatem repudiandae inventore quae veritatis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(45, 3, 'Est repellat tempora eius aut modi praesentium est.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(46, 3, 'Ullam eligendi quisquam quis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(47, 3, 'Dolor illum provident aut cum voluptatem.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(48, 3, 'Temporibus corporis laudantium est non.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(49, 3, 'Saepe eveniet recusandae laboriosam nemo sequi voluptas nam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(50, 3, 'Neque animi aliquid eos consequatur rerum fugiat.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(51, 3, 'Vel iusto voluptatibus sunt quasi quo molestiae.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(52, 3, 'Minus voluptatibus accusamus omnis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(53, 4, 'Explicabo iste voluptate et repellendus distinctio quisquam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(54, 4, 'Quas voluptatem enim sed.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(55, 4, 'Dicta est voluptates ipsum architecto quam amet quos.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(56, 4, 'Tempore magni et sed.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(57, 4, 'Reiciendis repudiandae nisi explicabo et saepe reprehenderit et.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(58, 4, 'Rem omnis laboriosam expedita eius culpa non dolor.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(59, 4, 'Incidunt aut sapiente nihil quasi iusto.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(60, 4, 'Unde sed commodi possimus nemo nam nihil et optio.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(61, 4, 'Dolores eius saepe magni dolorem.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(62, 4, 'Molestiae omnis ut suscipit animi qui est.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(63, 5, 'Voluptas et voluptatem corrupti soluta minima tempora cupiditate.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(64, 5, 'Inventore odio pariatur eum sequi.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(65, 5, 'Amet eius ipsum porro minima vero.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(66, 5, 'Laudantium at modi beatae rerum asperiores.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(67, 5, 'Eum sequi totam architecto dolores ex.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(68, 5, 'Voluptas praesentium quia voluptatem unde est.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(69, 5, 'Eius est molestiae delectus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(70, 5, 'Quia eos ipsa molestias facere eum.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(71, 5, 'Est iusto ipsa quia.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(72, 5, 'Rem autem aut mollitia sint culpa vero.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(73, 6, 'Qui architecto earum occaecati modi.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(74, 6, 'Sint enim nihil dolorum non.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(75, 6, 'Voluptatem nemo nulla quia et commodi quod.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(76, 6, 'Qui consectetur ut fugiat ipsa expedita.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(77, 6, 'Sit magni placeat velit ullam et.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(78, 6, 'Odio unde earum eaque neque perferendis aut perspiciatis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(79, 6, 'Nemo et enim magni itaque id.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(80, 6, 'Minus tenetur non aut accusamus ipsa harum voluptatem.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(81, 6, 'Eos nam cum consequatur et cum reprehenderit.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(82, 6, 'Non nisi quibusdam est tempora dignissimos sequi provident.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(83, 7, 'Et similique unde voluptates repudiandae aut.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(84, 7, 'Debitis consequuntur omnis molestiae praesentium laborum eos ab quaerat.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(85, 7, 'Sed deleniti sint asperiores reiciendis et asperiores doloremque.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(86, 7, 'Et sint dolor nemo cum ad repudiandae vitae.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(87, 7, 'Dolor tenetur voluptatem ex dignissimos qui tempore ducimus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(88, 7, 'Iusto recusandae omnis nesciunt dolores.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(89, 7, 'Sint sint fugit quis laudantium possimus.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(90, 7, 'Dolorum nisi ea consequatur officia aliquam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(91, 7, 'Sed in possimus quo quae voluptatem quam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(92, 7, 'Sint impedit quasi sequi impedit accusantium.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(93, 8, 'Omnis iusto atque quis adipisci et ad.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(94, 8, 'Sit ad iusto quia sequi optio.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(95, 8, 'Enim sequi quo voluptatum commodi nisi doloremque.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(96, 8, 'Facilis aspernatur dolorem aperiam vero.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(97, 8, 'Facilis dolor eligendi nihil repellendus aut.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(98, 8, 'Assumenda voluptatem aut labore et.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(99, 8, 'Magni soluta maiores autem rem et praesentium qui.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(100, 8, 'Aut et fugiat quo cum aperiam dicta.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(101, 8, 'Amet magni perferendis eaque dolorem facere libero.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(102, 8, 'Et quia vero sequi vero nam omnis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(103, 9, 'Quaerat ut totam reiciendis repellendus ratione repellendus eum.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(104, 9, 'Suscipit asperiores est temporibus eveniet necessitatibus eligendi.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(105, 9, 'Ea sunt consequuntur deleniti perspiciatis dignissimos aliquam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(106, 9, 'Est suscipit commodi totam adipisci consequatur.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(107, 9, 'Iure fugiat voluptas ut voluptatem voluptates consequatur.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(108, 9, 'Maiores et tempora fugit.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(109, 9, 'Et doloribus dolores ut molestiae animi ut.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(110, 9, 'Quaerat et consequatur qui nulla.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(111, 9, 'Repellendus blanditiis quae aut earum consequatur maxime.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(112, 9, 'Ut autem quo enim fugiat iure quis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(113, 10, 'Ullam rerum molestias quae.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(114, 10, 'Earum tempore eligendi doloribus a ab ullam corrupti.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(115, 10, 'Explicabo et quod qui voluptatibus odio quia totam veniam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(116, 10, 'Odit non soluta veniam.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(117, 10, 'Ut et tenetur voluptate illum porro.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(118, 10, 'Tempore voluptates nihil quia error est corporis voluptas iste.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(119, 10, 'Quia rerum eum et nemo tempore.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(120, 10, 'Est laboriosam ea quo quae consequatur quia eum.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(121, 10, 'Qui ut est iste quis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(122, 10, 'Numquam enim id sapiente et voluptas est.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(123, 11, 'Tempora quas non id eveniet est accusantium.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(124, 11, 'Dolore rerum accusantium itaque non et reiciendis asperiores.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(125, 11, 'Mollitia et voluptas ipsum cum officiis.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(126, 11, 'Nobis quo quos sed perferendis nisi facere quae.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(127, 11, 'Et deserunt itaque consequuntur.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(128, 11, 'Id velit quos asperiores rerum et totam tempora tempore.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(129, 11, 'Magnam est odio dolore sit beatae.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(130, 11, 'Quo et adipisci tempora voluptatem beatae eum saepe.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(131, 11, 'Quo minima quos dolor ea.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(132, 11, 'Non officia amet voluptatibus voluptatem maxime atque.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(133, 12, 'Voluptas perferendis reiciendis numquam mollitia at est cumque.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(134, 12, 'Quis sint quam et aliquam quo quod nihil mollitia.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(135, 12, 'Porro soluta qui natus consequuntur animi sequi soluta.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(136, 12, 'Et voluptatem delectus enim ullam nulla.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(137, 12, 'Asperiores dolor debitis unde laudantium consequuntur sit.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(138, 12, 'Velit voluptatem ipsum mollitia laboriosam necessitatibus nisi.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(139, 12, 'Autem et est ut ut officiis corrupti.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(140, 12, 'Sunt odit quos aut quia enim accusantium eum.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(141, 12, 'Quo accusamus quis debitis accusamus eius.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(142, 12, 'Qui quia qui illo iste eum in commodi itaque.', '2020-12-23 07:53:18', '2020-12-23 07:53:18'),
(143, 2, 'Hello', '2020-12-23 20:19:11', '2020-12-23 20:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`) USING HASH,
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `avatar`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'LinishSparks', 'Linish Sparks', 'avatars/atGIFZPpkqOKQ3vAHrFZIgGsLhxkW516H4dWiAPf.jpg', 'linish@mailinator.com', NULL, '$2y$10$ymoJcHtBnQOG59JF14PfUeAiBG5hRwQx/DlWWE2Pxvrl1SO0kRUeW', NULL, '2020-12-23 00:42:42', '2020-12-23 01:37:00'),
(2, 'linishriefmedias', 'Linish Riefmedias', 'avatars/Zf13zskoDUxa6SZtnavg8HUDdh5tOZMkbDLxbTzo.jpg', 'linish@riefmedia.com', NULL, '$2y$10$PhJv1dBc3tEY5FC3UUsdoe0yz3fg4xJl8XLoGxIgdvul3WCnzZ3Ay', NULL, '2020-12-23 06:23:05', '2020-12-23 20:19:00'),
(3, 'wilfredo09', 'Prof. Tito Paucek', NULL, 'heidi.orn@example.org', '2020-12-23 07:28:47', '$2y$10$sMYE6uoNiocaui2rHZW1H.4giJjYfGyyivz1TTWprV3ddKr6yX8Iy', 'pNyhLF1h4b', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(4, 'abbott.ida', 'Prof. Hayden Davis V', NULL, 'ijaskolski@example.org', '2020-12-23 07:28:47', '$2y$10$Z3x4RAXNkbnfb/nv1ZQ5E.5wdVqlmuVvQbOEgf8jX.tqkWXHPA6ru', 'j0FtdHORvO', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(5, 'cummerata.lonnie', 'Kavon Mueller', NULL, 'nbergnaum@example.net', '2020-12-23 07:28:47', '$2y$10$KQ5yBWSxuPneBkf6iWL66.5DzrG6fJJmBCEXRX2Y/ev/LRDpZG4AK', 'WLWRm9Ao6C', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(6, 'edgar.medhurst', 'Izabella Beer', NULL, 'cassidy57@example.org', '2020-12-23 07:28:47', '$2y$10$igay6hN.2YU.BT1eUpFlJ.szE2uLavZwQjBO/nUDLa18NxMrORL.S', 'rMV9DTgIjE', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(7, 'jwillms', 'Arthur Durgan', NULL, 'imogene84@example.org', '2020-12-23 07:28:47', '$2y$10$L6IL3em/RBZLY7jb.h6qp.zphAI5EAflSfVtHPNNA8TAB5UQMm18.', 'XgQmC2EoIo', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(8, 'stokes.soledad', 'Mr. Hilton Lehner Jr.', NULL, 'violette13@example.org', '2020-12-23 07:28:47', '$2y$10$q1AL9Ob5/Vjc/kqZ7ufO3eEQ10A9p51spyfC3Oq5fKC1nZb2ASRgS', 'fGdyVMwjNz', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(9, 'walter.gillian', 'Cortney Kohler', NULL, 'lehner.allan@example.org', '2020-12-23 07:28:47', '$2y$10$kmKQPfpQ2LU1O5CWi//B/OHf0cwsrk.QxHIDO8FwVfxqI2PLP8ixe', 'e8qWb7j620', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(10, 'franecki.donny', 'Darren Considine', NULL, 'isom57@example.com', '2020-12-23 07:28:47', '$2y$10$jAjUt46lWoknZRkUf4JHUOeHGYI2jDnJ3QSJ1YNn65prQHqICjcYm', 'lurvrKnbrs', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(11, 'bhane', 'Jamir Hartmann V', NULL, 'georgiana.pacocha@example.org', '2020-12-23 07:28:47', '$2y$10$wubWdohhJYwTM1rGQ4h72O.eoaiFnrWl6g6ulVGL/Tb5UTHHQi3Vy', 'gqWhd4ui0N', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(12, 'qkohler', 'Josiane Rohan', NULL, 'pschmidt@example.net', '2020-12-23 07:28:47', '$2y$10$wSwz6Ts.P0s7kD68VbFG2uXa82r5VwkL9kOnRlaWOLbmeUeoL8WnW', 'qpoWjyvGIa', '2020-12-23 07:28:47', '2020-12-23 07:28:47'),
(13, 'karolann.maggio', 'Jody Macejkovic', NULL, 'tstrosin@example.com', '2020-12-23 07:50:52', '$2y$10$IW91K2AIBKqVnM4RC/Ax9ei3YjOpypvIpiOof2CRVcSWXcKeHEpIa', 'Zq17grFJgl', '2020-12-23 07:50:52', '2020-12-23 07:50:52'),
(14, 'lillie36', 'Stanton Waters', NULL, 'zboncak.catherine@example.net', '2020-12-23 07:50:52', '$2y$10$Xri7UE58VxBLoS77ZXB4ieUW6iBMpUS4ZikFEV/k6NdJy2J0yExUS', 'lFu6lHe496', '2020-12-23 07:50:52', '2020-12-23 07:50:52'),
(15, 'sbailey', 'Dr. Shanny Jaskolski', NULL, 'israel83@example.net', '2020-12-23 07:50:52', '$2y$10$VPxLGlCN9or2H5r5UMGi7O1Ph7PpyY4W9PXOFwREPh5WM0V43WfMO', '79KCmujmgO', '2020-12-23 07:50:52', '2020-12-23 07:50:52'),
(16, 'beulah.mcglynn', 'Bernice Blanda', NULL, 'nikita.conroy@example.net', '2020-12-23 07:50:52', '$2y$10$qnUub8Z9skXYlBZ7a2phFeA45GyZnTtGFssuEAFBVw7yTnni0esZi', 'snNxO4ICg2', '2020-12-23 07:50:52', '2020-12-23 07:50:52'),
(17, 'laurel.hauck', 'Prof. Jensen Gulgowski DDS', NULL, 'hershel.borer@example.net', '2020-12-23 07:50:52', '$2y$10$.Tm74V1bGCcLa2RBfxZw3.Sa/wDoKLw.NuG8pRi1Nw1E9rozepY56', 'i5XJj886FD', '2020-12-23 07:50:53', '2020-12-23 07:50:53'),
(18, 'fay.lilla', 'Braulio Herzog', NULL, 'fkutch@example.com', '2020-12-23 07:50:53', '$2y$10$ohxlQ46aooGlcxJfPlUxletTLh8yLR6AUU0REAwv6QKm0F14AFE9O', 'JwkfshQeFr', '2020-12-23 07:50:53', '2020-12-23 07:50:53'),
(19, 'devan.grant', 'Mr. Alphonso Gibson', NULL, 'maxine91@example.org', '2020-12-23 07:50:53', '$2y$10$Ae3xvjQA8QeUHBQCzKxUGesm.2PAX084HbICQyED0maE3Vgc3Zspe', 'fcTXvdqspM', '2020-12-23 07:50:53', '2020-12-23 07:50:53'),
(20, 'cesar37', 'Jana Spinka', NULL, 'yreichel@example.org', '2020-12-23 07:50:53', '$2y$10$s0EtHexA7Yo.9UGTGYgMFuY1U1RMElj/2SPGJl.lFmk7CZBy7cpm.', '56t4466xjK', '2020-12-23 07:50:53', '2020-12-23 07:50:53'),
(21, 'orlando.wolf', 'Ms. Virgie McKenzie DDS', NULL, 'ekilback@example.com', '2020-12-23 07:50:53', '$2y$10$Wcu7M1TVC4EmCAgvLj3m4OG/.JAKikGFGa1zRjRLZdCPr14FiUOZy', 'vmxG1wTbnS', '2020-12-23 07:50:53', '2020-12-23 07:50:53'),
(22, 'pfritsch', 'Dr. Coy Turner', NULL, 'mpfannerstill@example.com', '2020-12-23 07:50:53', '$2y$10$lZx6x2rmi0FSiwqXAnz5i.LCx2WoDE3LOGYPkl0pWTWmVQXUO1d7W', 'rVJ3Yidcvy', '2020-12-23 07:50:53', '2020-12-23 07:50:53');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
