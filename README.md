## About Tweety

Twitter clone in Laravel 8

### To create symn link
> php artisan storage:link

### Tinker Commands
> php artisan tinker

### Example
> User::factory()->count(10)->create();


### Loop Example
> $users = User::all();
> 
> $users->each(function($user) { Tweet::factory()->count(10)->create(['user_id' => $user->id]); });

### Reference
[Laravel 8 Factory Tutorial](https://www.nicesnippets.com/blog/laravel-8-factory-example-tutorial)

### Code Reference & Tutorial
[Code Reference](https://github.com/laracasts/Tweety)

[Tutorial](https://laracasts.com/series/laravel-6-from-scratch)