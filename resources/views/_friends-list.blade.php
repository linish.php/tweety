<div class="bg-blue-100 border border-gray-300 rounded-2.5xl p-4">
    <h3 class="font-bold text-xl mb-4">Following</h3>

    <ul>
        @forelse (current_user()->follows as $user)
            <li class="{{ $loop->last ? '' : 'mb-4' }}">
                <div>
                    <a 
                        href="{{ $user->path() }}"
                        class="flex items-center text-sm"
                    >
                        <img 
                            src="{{ $user->avatar }}"
                            alt="John Doe"
                            class="rounded-full mr-2"
                            width="40"
                            height="40"
                        />
                        {{ $user->username }}
                    </a>
                </div>
            </li>
        @empty
            <li>
                No friends yet!
            </li>
        @endforelse
    </ul>
</div>