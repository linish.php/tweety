<x-master>
    <div class="container mx-auto flex justify-center">
        <div class="bg-gray-400 rounded py-8 px-12">
            <div class="col-md-8">
                <div class="font-bold text-lg mb-4">{{ __('Login') }}</div>

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="mb-6">
                        <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            Email
                        </label>
            
                        <input type="email" value="{{ old('email') }}" class="border boder-gray-400 p-2 w-full" name="email" id="email" required>
            
                        @error('email')
                            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            Password
                        </label>
            
                        <input type="password" class="border boder-gray-400 p-2 w-full" name="password" id="password" required>
            
                        @error('password')
                            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <div>
                            <input type="checkbox" class="mr-1" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                
                            <label for="remember" class="uppercase font-bold text-xs text-gray-700">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>

                    <div class="mb-6 flex justify-between items-center">
                        <button type="submit" class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500 mr-2">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-master>