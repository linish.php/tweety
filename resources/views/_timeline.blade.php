<div class="border border-gray-300 rounded-2.5xl">
    @forelse ($tweets as $tweet)                    
        @include('_tweet')
    @empty
        <p class="p-4">No tweets yet.</p>
    @endforelse
    <div class="px-4 py-4">
        {{ $tweets->links() }}
    </div> 
</div>