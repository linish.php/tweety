<div class="flex p-4 {{ $loop->last ? '' : 'border-b border-gray-300' }}">
    <div class="mr-2 flex-shrink-0">
        <a href="{{ route('profile', $tweet->user->name) }}">
            <img 
                src="{{ $tweet->user->avatar }}"
                alt="John Doe"
                class="rounded-full mr-2"
                width="50"
                height="50"
            />
        </a>
    </div>
    <div>
        <a href="{{ $tweet->user->path() }}">
            <h5 class="font-bold mb-4">{{ $tweet->user->name }}</h5>
        </a>
        <p class="text-sm mb-3">
            {{ $tweet->body }}
        </p>

        <x-like-buttons :tweet="$tweet" />
    </div>
</div>