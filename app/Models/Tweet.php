<?php

namespace App\Models;

use App\Traits\Likable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tweet extends Model
{
    use HasFactory, Likable;

    public $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
