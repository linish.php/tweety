<?php
namespace App\Traits;

use App\Models\Like;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait Likable 
{

    public function scopeWithLikes(Builder $query)
    {
        // SELECT * FROM `tweets` LEFT JOIN (SELECT `tweet_id`, SUM(`liked`) as likes, SUM(!`liked`) as dislikes FROM `likes` GROUP BY `tweet_id`) `likes` on `likes`.`tweet_id` = `tweets`.`id`
        return $query->leftJoinSub(
            'SELECT `tweet_id`, SUM(`liked`) as likes, SUM(!`liked`) as dislikes FROM `likes` GROUP BY `tweet_id`',
            'likes', 
            'likes.tweet_id',
            'tweets.id'
        );        
    }

    public function like($user = null, $liked = true)
    {
        $this->likes()->updateOrCreate([
            'user_id' => $user ? $user->id : auth()->id()
        ], [
            'liked' => $liked
        ]);
    }

    public function dislike($user = null)
    {
        return $this->like($user, false);
    }

    public function isLikedBy(User $user)
    {
        // return $this->likes()->where('user_id', $user->id)->exists() // n+1 problem inside a loop
        return (bool) $user->likes->where('tweet_id', $this->id)->where('liked', true)->count(); // Load only specific user likes
    }

    public function isDislikedBy(User $user)
    {
        return (bool) $user->likes->where('tweet_id', $this->id)->where('liked', false)->count(); // Load only specific user likes
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }    
}