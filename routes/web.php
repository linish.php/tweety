<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TweetsController;
use App\Http\Controllers\ExploreController;
use App\Http\Controllers\FollowsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TweetLikesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function() {
    Route::get('/tweets', [TweetsController::class, 'index'])->name('home');
    Route::post('/tweets', [TweetsController::class, 'store']);    

    Route::post('/tweets/{tweet}/like', [TweetLikesController::class, 'store']);
    Route::delete('/tweets/{tweet}/like', [TweetLikesController::class, 'destroy']);

    Route::post('/profile/{user}/follow', [FollowsController::class, 'store'])->name('follow');
    Route::get('/profile/{user}/edit', [ProfileController::class, 'edit'])->middleware('can:edit,user');
    Route::patch('/profile/{user}', [ProfileController::class, 'update'])->middleware('can:edit,user');

    Route::get('/explore', ExploreController::class); // Invokable controller

});

Route::get('/profile/{user}', [ProfileController::class, 'show'])->name('profile');


Auth::routes();